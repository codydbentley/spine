
import PluginMessage from "./templates/plugin-message";

const Canvas2d = (function() {

    let name = "Canvas";
    let instance;

    const init = (engine) => {

        let height, width, ctx;

        height = engine.container.clientHeight;
        width = engine.container.clientWidth;
        let c = document.createElement("canvas");
        c.style.position = "absolute";
        c.height = height;
        c.width = width;
        ctx = c.getContext("2d");
        engine.container.appendChild(c);

        const api = () => {

            let renderer;

            let plugin = {
                render: (args) => {
                    ctx.clearRect(0, 0, width, height);
                    ctx.putImageData(args.pixels, 0, 0);
                }
            };

            const RenderPlugin = (uri) => {
                renderer = new Worker(uri);
                renderer.postMessage(new PluginMessage("register", {
                    height,
                    width
                }));

                renderer.onmessage = (e) => {
                    if (plugin.hasOwnProperty(e.data.handler)) {
                        plugin[e.data.handler](e.data.args);
                    }
                };
            };

            const Clear = () => {
                renderer.postMessage(new PluginMessage("clear", {
                    r: 0,
                    g: 0,
                    b: 0,
                    a: 255
                }));
            };

            const Draw = (xp, yp) => {
                renderer.postMessage(new PluginMessage("draw", {
                    xp,
                    yp
                }));
            };

            const Render = () => {
                renderer.postMessage(new PluginMessage("render"));
            };

            return {
                RenderPlugin,
                Clear,
                Draw,
                Render
            }
        };

        return {
            name,
            api: api()
        }
    };

    const Use = (engine) => {
        if (!instance) {
            instance = init(engine);
        }
        return instance;
    };

    return {
        Use
    }
}());

export default Canvas2d;