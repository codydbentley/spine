
const Matrix4 = (function() {

    const Projection = (w,h) => {
        return [
            2/w, 0, 0,
            0, -2/h, 0,
            -1, 1, 1,
        ];
    };

    const Orthographic = (l, r, b, t, n, f, a) => {
        a = a || new Float32Array(16);
        a[0] = 2 / (r - l);
        a[1] = 0;
        a[2] = 0;
        a[3] = 0;
        a[4] = 0;
        a[5] = 2 / (t - b);
        a[6] = 0;
        a[7] = 0;
        a[8] = 0;
        a[9] = 0;
        a[10] = 2 / (n - f);
        a[11] = 0;
        a[12] = (l + r) / (l - r);
        a[13] = (b + t) / (b - t);
        a[14] = (n + f) / (n - f);
        a[15] = 1;
        return a;
    };

    const Translation = (x, y, z, a) => {
        a = a || new Float32Array(16);
        a[0] = 1;
        a[1] = 0;
        a[2] = 0;
        a[3] = 0;
        a[4] = 0;
        a[5] = 1;
        a[6] = 0;
        a[7] = 0;
        a[8] = 0;
        a[9] = 0;
        a[10] = 1;
        a[11] = 0;
        a[12] = x;
        a[13] = y;
        a[14] = z;
        a[15] = 1;
        return a;
    };

    const Scaling = (x, y, z, a) => {
        a = a || new Float32Array(16);
        a[0] = x;
        a[1] = 0;
        a[2] = 0;
        a[3] = 0;
        a[4] = 0;
        a[5] = y;
        a[6] = 0;
        a[7] = 0;
        a[8] = 0;
        a[9] = 0;
        a[10] = z;
        a[11] = 0;
        a[12] = 0;
        a[13] = 0;
        a[14] = 0;
        a[15] = 1;
        return a;
    };

    const Translate = (m, x, y, z, a) => {
        a = a || new Float32Array(16);
        let m00 = m[0];
        let m01 = m[1];
        let m02 = m[2];
        let m03 = m[3];
        let m10 = m[4];
        let m11 = m[5];
        let m12 = m[6];
        let m13 = m[7];
        let m20 = m[8];
        let m21 = m[9];
        let m22 = m[10];
        let m23 = m[11];
        let m30 = m[12];
        let m31 = m[13];
        let m32 = m[14];
        let m33 = m[15];
        if (m !== a) {
            a[0] = m00;
            a[1] = m01;
            a[2] = m02;
            a[3] = m03;
            a[4] = m10;
            a[5] = m11;
            a[6] = m12;
            a[7] = m13;
            a[8] = m20;
            a[9] = m21;
            a[10] = m22;
            a[11] = m23;
        }
        a[12] = m00 * x + m10 * y + m20 * z + m30;
        a[13] = m01 * x + m11 * y + m21 * z + m31;
        a[14] = m02 * x + m12 * y + m22 * z + m32;
        a[15] = m03 * x + m13 * y + m23 * z + m33;
        return a;
    };

    const Scale = (m, x, y, z, a) => {
        a = a || new Float32Array(16);
        a[0] = x * m[0];
        a[1] = x * m[1];
        a[2] = x * m[2];
        a[3] = x * m[3];
        a[4] = y * m[4];
        a[5] = y * m[5];
        a[6] = y * m[6];
        a[7] = y * m[7];
        a[8] = z * m[8];
        a[9] = z * m[9];
        a[10] = z * m[10];
        a[11] = z * m[11];
        if (m !== a) {
            a[12] = m[12];
            a[13] = m[13];
            a[14] = m[14];
            a[15] = m[15];
        }
        return a;
    };

    return {
        Projection,
        Orthographic,
        Translation,
        Scaling,
        Translate,
        Scale,
    }
}());

export default Matrix4;