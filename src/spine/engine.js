
const Engine = (function() {

    let instance;
    let submodules = [];

    // core engine pieces
    let engine = {
        container: null,
        event: null,
        render: () => {},
    };

    const init = (opts, engine) => {

        opts = opts || {};

        let defaultOpts = {
            container: "",
            tick: 60,
        };

        let finalOpts = {...defaultOpts, ...opts};

        if (!finalOpts.container) {
            throw new Error("Options must have a container selector to bind to.");
        }

        // base engine
        let lastTime;
        let tick = 1/finalOpts.tick;
        let delta;
        let timer;
        let frames;
        let frameCount;
        let fps;
        let frameId;

        // engine dom container
        engine.container = document.querySelector(finalOpts.container);

        // event bus
        engine.event = buildBus(engine.container);

        // setup
        engine.container.style.padding = "0";
        engine.container.style.position = "relative";

        const render = (delta) => {
            engine.render(delta);
        };

        const run = (ts) => {
            delta += (ts - lastTime) / 1000;
            lastTime = ts;
            if (delta > tick) {
                delta -= tick;
                render(delta, engine.render);
                nextFrame();
            }
            if (ts - timer > 1000) {
                timer += 1000;
                fps = frames;
                frames = 0;
                console.log("fps: %d (%d)", fps, frameCount);
            }
            frameId = requestAnimationFrame(run);
        };

        const nextFrame = () => {
            frames++;
            frameCount++;
        };

        const Start = () => {
            delta = 0;
            frames = 0;
            frameCount = 0;
            requestAnimationFrame(ts => {
                lastTime = ts;
                timer = ts;
                run(ts);
            });
        };

        const Stop = () => {
            cancelAnimationFrame(frameId);
        };

        let engineInstance = {
            Events: engine.event,
            Start,
            Stop
        };

        submodules.forEach(submodule => {
            module = submodule.Use(engine);
            engineInstance[module.name] = module.api;
        });

        return engineInstance;
    };

    // engine event bus
    const buildBus = (container) => {
        let bus = {
            registry: {},
            handlers: {}
        };

        let currentHandlerId = 0;

        const Emit = (eventName, eventData) => {
            let ev = new CustomEvent(eventName, {detail: eventData});
            container.dispatchEvent(ev);
        };

        const Listen = (eventName, handler) => {
            if (typeof handler !== "function") {
                throw new Error("Handler must be of type function.");
            }
            if (!bus.registry.hasOwnProperty(eventName)) {
                bus.registry[eventName] = {};
            }
            let handlerId = currentHandlerId++;
            bus.registry[eventName][handlerId] = handler;
            rebuild(eventName);
            return handlerId;
        };

        const RemoveListener = (eventName, handlerId) => {
            if (bus.registry.hasOwnProperty(eventName)) {
                if (bus.registry[eventName].hasOwnProperty(handlerId)) {
                    delete bus.registry[eventName][handlerId];
                }
            }
            if (Object.keys(bus.registry[eventName]).length < 1) {
                delete bus.registry[eventName];
            }
            rebuild(eventName);
        };

        const rebuild = (eventName) => {
            if (bus.handlers.hasOwnProperty(eventName)) {
                container.removeEventListener(eventName, bus.handlers[eventName]);
            }
            if (bus.registry.hasOwnProperty(eventName)) {
                bus.handlers[eventName] = (e) => {
                    e.preventDefault();
                    let handlers = Object.keys(bus.registry[eventName]);
                    handlers.forEach(handler => {
                        bus.registry[eventName][handler](e.detail);
                    });
                };
                container.addEventListener(eventName, bus.handlers[eventName]);
            }
        };

        return {
            Emit,
            Listen,
            RemoveListener
        };
    };

    const Use = (submodule) => {
        submodules.push(submodule)
    };

    const getEngine = (opts) => {
        if (!instance) {
            instance = init(opts, engine);
        }
        return instance;
    };

    return {
        Use,
        Engine: getEngine,
    }
}());

export default Engine;