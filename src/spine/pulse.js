
import PluginMessage from "./templates/plugin-message";

const Pulse = (function() {

    let name = "Pulse";
    let instance;

    const init = (engine) => {

        const api = () => {

            let hb;

            let plugin = {

                update: (args) => {
                    engine.event.Emit("update", args);
                }
            };

            const HeartbeatPlugin = (uri) => {
                hb = new Worker(uri);
                hb.postMessage(new PluginMessage("register", {tickRate: 60}));

                hb.onmessage = (e) => {
                    if (plugin.hasOwnProperty(e.data.handler)) {
                        plugin[e.data.handler](e.data.args);
                    }
                };
            };

            const Start = () => {
                hb.postMessage(new PluginMessage("start"));
            };

            const Stop = () => {
                hb.postMessage(new PluginMessage("stop"));
            };

            return {
                HeartbeatPlugin,
                Start,
                Stop
            }
        };

        return {
            name,
            api: api()
        }
    };

    const Use = (engine) => {
        if (!instance) {
            instance = init(engine);
        }
        return instance;
    };

    return {
        Use
    }
}());

export default Pulse;