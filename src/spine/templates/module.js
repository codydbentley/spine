
import PluginMessage from "./plugin-message";

const ModuleName = (function() {

    // Name that will get bound, ex- Spine.Module
    let name = "Module";
    let instance;

    const init = (engine) => {

        // do module initialization here.
        // by this step, engine should be initialized and ready for use

        const api = () => {

            const PublicMethod1 = () => {
                // do things
                // has access to engine
            };

            const PublicMethod2 = () => {
                // do things
                // has access to engine
            };

            // exposed methods, ex- Spine.Module.PublicMethod1()
            return {
                PublicMethod1,
                PublicMethod2,
            }
        };

        return {
            name,
            api: api()
        }
    };

    const Use = (engine) => {
        if (!instance) {
            instance = init(engine);
        }
        return instance;
    };

    return {
        Use
    }
}());

export default ModuleName;