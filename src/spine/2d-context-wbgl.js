
import Matrix4 from "./utils/matrix";

const Canvas2dWebGL = (function() {

    let name = "Screen";
    let instance;

    let vertexSource = `
        attribute vec4 a_position;
        attribute vec2 a_texcoord;
        
        uniform mat4 u_matrix;
        uniform mat4 u_textureMatrix;
        
        varying vec2 v_texcoord;
        
        void main() {
           gl_Position = u_matrix * a_position;
           v_texcoord = (u_textureMatrix * vec4(a_texcoord, 0, 1)).xy;
        }
    `;

    let fragmentSource = `
        precision mediump float;
        
        varying vec2 v_texcoord;
        
        uniform sampler2D u_texture;
        
        void main() {
          if (v_texcoord.x < 0.0 ||
            v_texcoord.y < 0.0 ||
            v_texcoord.x > 1.0 ||
            v_texcoord.y > 1.0) {
            discard;
          }
          gl_FragColor = texture2D(u_texture, v_texcoord);
        }
    `;

    const createShader = (gl, type, src) => {
        let shader = gl.createShader(type);
        gl.shaderSource(shader, src);
        gl.compileShader(shader);

        let success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
        if (!success) {
            let info = gl.getShaderInfoLog(shader);
            gl.deleteShader(shader);
            throw new Error("Shader error: \n" + info);
        }
        return shader;
    };

    const createShaderProgram = (gl) => {
        let vShader = createShader(gl, gl.VERTEX_SHADER, vertexSource);
        let fShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentSource);
        let program = gl.createProgram();
        gl.attachShader(program, vShader);
        gl.attachShader(program, fShader);
        gl.linkProgram(program);

        let success = gl.getProgramParameter(program, gl.LINK_STATUS);
        if (!success) {
            let info = gl.getProgramInfoLog(program);
            gl.deleteProgram(program);
            throw new Error("Program error:\n" + info);
        }

        return program;
    };

    const orthographic = (l, r, b, t, n, f, a) => {
        a = a || new Float32Array(16);
        a[0] = 2 / (r - l);
        a[1] = 0;
        a[2] = 0;
        a[3] = 0;
        a[4] = 0;
        a[5] = 2 / (t - b);
        a[6] = 0;
        a[7] = 0;
        a[8] = 0;
        a[9] = 0;
        a[10] = 2 / (n - f);
        a[11] = 0;
        a[12] = (l + r) / (l - r);
        a[13] = (b + t) / (b - t);
        a[14] = (n + f) / (n - f);
        a[15] = 1;
        return a;
    };

    const translation = (x, y, z, a) => {
        a = a || new Float32Array(16);
        a[0] = 1;
        a[1] = 0;
        a[2] = 0;
        a[3] = 0;
        a[4] = 0;
        a[5] = 1;
        a[6] = 0;
        a[7] = 0;
        a[8] = 0;
        a[9] = 0;
        a[10] = 1;
        a[11] = 0;
        a[12] = x;
        a[13] = y;
        a[14] = z;
        a[15] = 1;
        return a;
    };

    const translate = (m, x, y, z, a) => {
        a = a || new Float32Array(16);
        let m00 = m[0];
        let m01 = m[1];
        let m02 = m[2];
        let m03 = m[3];
        let m10 = m[4];
        let m11 = m[5];
        let m12 = m[6];
        let m13 = m[7];
        let m20 = m[8];
        let m21 = m[9];
        let m22 = m[10];
        let m23 = m[11];
        let m30 = m[12];
        let m31 = m[13];
        let m32 = m[14];
        let m33 = m[15];
        if (m !== a) {
            a[0] = m00;
            a[1] = m01;
            a[2] = m02;
            a[3] = m03;
            a[4] = m10;
            a[5] = m11;
            a[6] = m12;
            a[7] = m13;
            a[8] = m20;
            a[9] = m21;
            a[10] = m22;
            a[11] = m23;
        }
        a[12] = m00 * x + m10 * y + m20 * z + m30;
        a[13] = m01 * x + m11 * y + m21 * z + m31;
        a[14] = m02 * x + m12 * y + m22 * z + m32;
        a[15] = m03 * x + m13 * y + m23 * z + m33;
        return a;
    };

    const scale = (m, x, y, z, a) => {
        a = a || new Float32Array(16);
        a[0] = x * m[0];
        a[1] = x * m[1];
        a[2] = x * m[2];
        a[3] = x * m[3];
        a[4] = y * m[4];
        a[5] = y * m[5];
        a[6] = y * m[6];
        a[7] = y * m[7];
        a[8] = z * m[8];
        a[9] = z * m[9];
        a[10] = z * m[10];
        a[11] = z * m[11];
        if (m !== a) {
            a[12] = m[12];
            a[13] = m[13];
            a[14] = m[14];
            a[15] = m[15];
        }
        return a;
    };

    const init = (engine) => {

        let height, width;

        let gl, program, posLoc, texcoordLoc, matLoc, texMatLoc, texLoc, posBuffer;


        let canvas = document.createElement("canvas");
        canvas.style.position = "absolute";
        gl = canvas.getContext("webgl");
        engine.container.appendChild(canvas);

        // setup webgl
        program = createShaderProgram(gl);

        posLoc = gl.getAttribLocation(program, "a_position");
        texcoordLoc = gl.getAttribLocation(program, "a_texcoord");

        matLoc = gl.getUniformLocation(program, "u_matrix");
        texMatLoc = gl.getUniformLocation(program, "u_textureMatrix");
        texLoc = gl.getUniformLocation(program, "u_texture");

        posBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, posBuffer);

        let pos = [
            0, 0,
            0, 1,
            1, 0,
            1, 0,
            0, 1,
            1, 1,
        ];
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(pos), gl.STATIC_DRAW);

        let texcoordBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);

        let texcoords = [
            0, 0,
            0, 1,
            1, 0,
            1, 0,
            0, 1,
            1, 1,
        ];
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(texcoords), gl.STATIC_DRAW);

        gl.clearColor(0,0,0, 1.0);

        const resize = (canvas) => {
            width = engine.container.clientWidth;
            height = engine.container.clientHeight;
            if (canvas.width !== height || canvas.width !== width) {
                canvas.height = height;
                canvas.width = width;
            }
        };

        const api = () => {

            const LoadSprite = (url) => {
                let tx = gl.createTexture();
                gl.bindTexture(gl.TEXTURE_2D, tx);
                gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array([0, 0, 255, 255]));

                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

                let sprite = {
                    w: 1,
                    h: 1,
                    tx: tx,
                };

                let img = new Image();
                img.addEventListener('load', () => {
                    sprite.w = img.width;
                    sprite.h = img.height;
                    gl.bindTexture(gl.TEXTURE_2D, sprite.tx);
                    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img);
                });
                img.src = url;
                return sprite;
            };

            const Draw = (
                sprite, spriteWidth, spriteHeight,
                srcX, srcY, srcWidth, srcHeight,
                targetX, targetY, targetWidth, targetHeight
            ) => {
                srcX = srcX || 0;
                srcY = srcY || 0;
                targetX = targetX || 0;
                targetY = targetY || 0;
                srcWidth = srcWidth || spriteWidth;
                srcHeight = srcHeight || spriteHeight;
                targetWidth = targetWidth || spriteWidth;
                targetHeight = targetHeight || spriteHeight;

                gl.bindTexture(gl.TEXTURE_2D, sprite);
                gl.useProgram(program);

                gl.bindBuffer(gl.ARRAY_BUFFER, posBuffer);
                gl.enableVertexAttribArray(posLoc);
                gl.vertexAttribPointer(posLoc, 2, gl.FLOAT, false, 0, 0);

                gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);
                gl.enableVertexAttribArray(texcoordLoc);
                gl.vertexAttribPointer(texcoordLoc, 2, gl.FLOAT, false, 0, 0);

                let m = orthographic(0, width, height, 0, -1, 1);
                m = translate(m, targetX, targetY, 0);
                m = scale(m, targetWidth, targetHeight, 1);
                gl.uniformMatrix4fv(matLoc, false, m);

                let tm = translation(srcX/spriteWidth, srcY/spriteHeight, 0);
                tm = scale(tm, srcWidth/spriteWidth, srcHeight/spriteHeight, 1);
                gl.uniformMatrix4fv(texMatLoc, false, tm);

                gl.uniform1i(texLoc, 0);

                gl.enable(gl.BLEND);
                gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

                gl.drawArrays(gl.TRIANGLES, 0, 6);
            };

            const Render = (ops) => {
                engine.render = (delta) => {
                    resize(gl.canvas);
                    gl.viewport(0, 0, width, height);
                    gl.clear(gl.COLOR_BUFFER_BIT);
                    ops(delta);
                };
            };

            return {
                LoadSprite,
                Draw,
                Render,
            }
        };

        return {
            name,
            api: api()
        }
    };

    const Use = (engine) => {
        if (!instance) {
            instance = init(engine);
        }
        return instance;
    };

    return {
        Use
    }
}());

export default Canvas2dWebGL;