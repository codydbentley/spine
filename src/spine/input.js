
const Input = (function() {

    let name = "Input";
    let instance;

    let keys = {};

    let KeyInput = {
        Up: false,
        Down: false,
        Left: false,
        Right: false
    };

    const init = (engine) => {

        let inputElement;

        inputElement = document.createElement("div");
        engine.container.appendChild(inputElement);
        inputElement.style.position = "absolute";
        inputElement.style.top = "0";
        inputElement.style.right = "0";
        inputElement.style.bottom = "0";
        inputElement.style.left = "0";
        inputElement.tabIndex = 0;
        inputElement.style.zIndex = "10";
        inputElement.style.outline = "none";
        inputElement.focus();

        const api = () => {

            const BindKeyboard = () => {
                inputElement.addEventListener('keydown', (e) => {
                    e.preventDefault();
                    keys[e.key] = true;
                });
                inputElement.addEventListener('keyup', (e) => {
                    e.preventDefault();
                    keys[e.key] = false;
                });
            };

            const BindMouse = () => {
                inputElement.addEventListener('click', (e) => {
                    e.preventDefault();
                    console.log(e);
                });
            };

            const BindGamepads = () => {
                inputElement.addEventListener('gamepadconnected', (e) => {
                    e.preventDefault();
                    console.log(e);
                });
            };

            const Update = () => {
                Input.Up = Boolean(keys["w"] || keys["ArrowUp"]);
                Input.Down = Boolean(keys["s"] || keys["ArrowDown"]);
                Input.Left = Boolean(keys["a"] || keys["ArrowLeft"]);
                Input.Right = Boolean(keys["d"] || keys["ArrowRight"]);
            };

            const GetInput = () => {
                return Input;
            };

            return {
                BindKeyboard,
                BindMouse,
                BindGamepads,
                Update,
                GetInput,
            }
        };

        return {
            name,
            api: api()
        }
    };

    const Use = (engine) => {
        if (!instance) {
            instance = init(engine);
        }
        return instance;
    };

    return {
        Use
    }
}());

export default Input;