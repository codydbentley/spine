
import Spine from '../spine/engine';
import Pulse from "../spine/pulse";
import Canvas2dWebGL from "../spine/2d-context-wbgl";
import Input from "../spine/input";

Spine.Use(Pulse);
Spine.Use(Canvas2dWebGL);
Spine.Use(Input);

let game = Spine.Engine({
    container: "#example",
});

game.Pulse.HeartbeatPlugin('plugins/heartbeat.js');

game.Input.BindKeyboard();
// example.Input.BindMouse();
// example.Input.BindGamepads();

let x = 0;
let y = 0;
let movement = 5;

game.Events.Listen("update", () => {
    game.Input.Update();
    let input = game.Input.GetInput();
    if (input.Up) {
        y -= movement;
    }
    if (input.Down) {
        y += movement;
    }
    if (input.Left) {
        x -= movement;
    }
    if (input.Right) {
        x += movement;
    }
});

let mySprite = game.Screen.LoadSprite('assets/sprites/1-2-3.png');

game.Screen.Render(ts => {
    let w = mySprite.w;
    let h = mySprite.h;
    // sprite, spriteWidth, spriteHeight, srcX, srcY, srcWidth, srcHeight, targetX, targetY, targetWidth, targetHeight
    // game.Screen.Draw(mySprite.tx, w, h);
    game.Screen.Draw(mySprite.tx, w, h, 0, 0, 128, 128, x, y, 128, 128);
    game.Screen.Draw(mySprite.tx, w, h, 128, 0, 128, 128, x+64, y+64, 128, 128);
    game.Screen.Draw(mySprite.tx, w, h, 256, 0, 128, 128, x+128, y+128, 512, 512);
});

game.Pulse.Start();
game.Start();