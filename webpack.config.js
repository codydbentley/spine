const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: './src/example/index.js',
    output: {
        filename: 'js/example.js',
        path: path.resolve(__dirname, 'dist'),
    },
    watch: true,
    devtool: false,
    plugins: [
        new webpack.SourceMapDevToolPlugin({})
    ]
};